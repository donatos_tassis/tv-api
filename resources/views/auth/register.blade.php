@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header" style="color: #fff; background-color: #3e0e8b; font-weight: bold">
                    {{ __('Register') }}
                </div>

                <div class="card-body">
                    <register-form></register-form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
