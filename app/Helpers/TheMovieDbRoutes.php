<?php


namespace App\Helpers;

/**
 * Helper Class that contains all the routes for TheMovieDb API
 *
 * Class TheMovieDbRoutes
 *
 * @package App\Helpers
 */
class TheMovieDbRoutes
{
    /** @const string  */
    const API_BASE_URL = 'https://api.themoviedb.org';

    /** @const string */
    const API_VERSION = '/3';

//    /** @const string  */
//    const LOGIN = '/login';

    /** @const string  */
    const SEARCH_SERIES = self::API_VERSION.'/search/tv';

    /** @const string  */
    const DISCOVER_SERIES = self::API_VERSION.'/discover/tv';

    /** @const string  */
    const POPULAR_SERIES = self::API_VERSION.'/tv/popular';

    /** @const string  */
    const TOP_RATED_SERIES = self::API_VERSION.'/tv/top_rated';

    /** @const string  */
    const GENRE_CATEGORIES = self::API_VERSION.'/genre/tv/list';

    /** @const string  */
    const SERIES = self::API_VERSION.'/tv/';

    /** @const string  */
    const SEASON = '/season/';

    /** @const string  */
    const TRAILER = '/videos';

//    /** @const string  */
//    const UPDATED_SERIES = '/updated/query';
}
