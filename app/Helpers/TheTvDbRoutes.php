<?php


namespace App\Helpers;

/**
 * Helper Class that contains all the routes for TheTvDb API
 *
 * Class TheTvDbRoutes
 *
 * @package App\Helpers
 */
class TheTvDbRoutes
{
    /** @const string  */
    const API_BASE_URL = 'https://api.thetvdb.com';

    /** @const string  */
    const LOGIN = '/login';

    /** @const string  */
    const SEARCH_SERIES = '/search/series';

    /** @const string  */
    const SERIES = '/series/';

    /** @const string  */
    const EPISODES = '/episodes';

    /** @const string  */
    const UPDATED_SERIES = '/updated/query';
}
