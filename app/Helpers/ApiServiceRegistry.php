<?php


namespace App\Helpers;


use App\Services\ApiServiceInterface;
use http\Exception\InvalidArgumentException;

class ApiServiceRegistry
{
    protected $services = [];

    public function register($name, ApiServiceInterface $instance) {
        $this->services[$name] = $instance;
        return $this;
    }

    public function get($name) {
        if (array_key_exists($name, $this->services)) {
            return $this->services[$name];
        } else {
            throw new InvalidArgumentException("Invalid service");
        }
    }
}
