<?php

namespace App\Console\Commands;

use App\Helpers\TheTvDbRoutes;
use App\Services\TheTvDbClient;
use App\Services\TheTvDbService;
use Illuminate\Console\Command;
use Illuminate\Http\JsonResponse;

/**
 * Command that updates series and their episodes that have been updated during the last 24 hours
 *
 * Class SeriesUpdateCommand
 *
 * @package App\Console\Commands
 */
class SeriesUpdateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'series:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Gets the latest series updates and updates the corresponding entity rows'.
     ' both for series and episodes entities';

    /** @var TheTvDbClient $client */
    protected $client;

    /** @var TheTvDbService $theTvDbService */
    protected $service;

    /**
     * Create a new command instance.
     *
     * @param TheTvDbClient $client
     * @param TheTvDbService $service
     */
    public function __construct(TheTvDbClient $client, TheTvDbService $service)
    {
        parent::__construct();
        $this->client = $client;
        $this->service = $service;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // EPOCH time for yesterday (exactly 24hrs in the past)
        $yesterday = time() - ( 1 * 24 * 60 * 60 );
        /** @var JsonResponse $response */
        $response = $this->client->requestAPI(
            TheTvDbRoutes::UPDATED_SERIES,
            ['query' => ['fromTime' => $yesterday]]
        );

        $responseData = json_decode($response->getOriginalContent(), true);
        $series = $responseData['data'];

        foreach ($series as $seriesItem)
        {
            $this->service->series($seriesItem['id']);
            $this->service->episodes($seriesItem['id']);
        }
    }
}
