<?php


namespace App\Services;

use App\Helpers\TheTvDbRoutes;
use App\Repositories\Interfaces\EpisodesRepositoryInterface;
use App\Repositories\Interfaces\SeriesRepositoryInterface;
use App\Series;
use GuzzleHttp\Client;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * Class TheTvDbService
 *
 * @package App\Services
 */
class TheTvDbService implements ApiServiceInterface
{
    /** @var TheTvDbClient $client */
    protected $client;

    /** @var SeriesRepositoryInterface $seriesRepository */
    private $seriesRepository;

    /** @var EpisodesRepositoryInterface $episodesRepository */
    private $episodesRepository;

    /**
     * TheTvDbService constructor.
     *
     * @param Client $client
     * @param SeriesRepositoryInterface $seriesRepository
     * @param EpisodesRepositoryInterface $episodesRepository
     */
    public function __construct(
        Client $client,
        SeriesRepositoryInterface $seriesRepository,
        EpisodesRepositoryInterface $episodesRepository
    ) {
        $this->client = $client;
        $this->seriesRepository = $seriesRepository;
        $this->episodesRepository = $episodesRepository;
    }

    /**
     * Search series by name
     * the name must by passed as query parameter of the request
     *
     * @param Request $request
     *
     * @return array|mixed
     */
    public function searchSeries(Request $request)
    {
        $name = $request->input('name');
        $response = $this->client->requestAPI(
            TheTvDbRoutes::SEARCH_SERIES,
            ['query' => ['name' => $name]]
        );

        return $response->getOriginalContent();
    }

    /**
     * List of all episodes for the specified series id
     *
     * @param Request $request
     * @param int $id           -   the series id
     * @param int $season       -   the season id
     * @param int $page         -   the page of the results
     * @param bool $addEpisodes -   boolean flag that determines if the database should be updated with new episodes
     *
     * @return array|mixed
     */
    public function episodes(Request $request, $id, $season = 0, $page = 1, $addEpisodes = true)
    {
        $series = Series::find($id);
        if(!$series)
            return [];

        /** @var JsonResponse $response */
        $response = $this->client->requestAPI(
            TheTvDbRoutes::SERIES . $id . TheTvDbRoutes::EPISODES,
            ['query' => ['page' => $page]]
        );

        if($response->status() === 200 && $addEpisodes)
        {
            $pageData = json_decode($response->getOriginalContent(), true);
            $episodes = $pageData['data'];

            foreach ($episodes as $episode)
                $this->episodesRepository->addEpisode($episode);

            $next = $pageData['links']['next']; // link to the next page of episodes
            if($next)
                $this->episodes($id, $next);
        }

        return $response->getOriginalContent();
    }

    /**
     * Get series information by id
     *
     * @param Request $request
     * @param int $id               -   the series id
     * @param bool $addSeries       -   boolean flag that determines if the database should be updated with new series
     *
     * @return array|mixed
     */
    public function series(Request $request, $id, $addSeries = true)
    {
        /** @var JsonResponse $response */
        $response = $this->client->requestAPI(TheTvDbRoutes::SERIES . $id);

        if($response->status() === 200 && $addSeries)
        {
            $responseData = json_decode($response->getOriginalContent(), true);
            $series = $responseData['data'];
            $this->seriesRepository->addSeries($series);
        }

        return $response->getOriginalContent();
    }

    public function popular(Request $request)
    {
        // TODO: Implement popular() method.
    }

    public function topRated(Request $request)
    {
        // TODO: Implement topRated() method.
    }

    public function genreCategories(Request $request)
    {
        // TODO: Implement genreCategories() method.
    }

    public function filter(Request $request)
    {
        // TODO: Implement filter() method.
    }

    public function videos(Request $request, $id)
    {
        // TODO: Implement trailer() method.
    }
}
