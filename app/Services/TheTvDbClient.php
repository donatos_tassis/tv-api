<?php


namespace App\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

/**
 * Class TheTvDbClient
 *
 * @package App\Services
 */
class TheTvDbClient extends Client implements ApiClientInterface
{
    /** @var Client $client */
    protected $client;

    /**
     * TheTvDbClient constructor.
     *
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        parent::__construct();
        $this->client = $client;
    }

    /**
     * Request to TheTvDb API Client
     *
     * @param string $url
     * @param array $options
     * @param string $method
     *
     * @return array|mixed
     */
    public function requestAPI($url, $options = [], $method = 'GET')
    {
        try {
            /** @var Response $response */
            $response = $this->client->request($method, $url, $options);
        } catch (ClientException $error) {
            $input = $error->getMessage();
            // get the string between the 2 line breaks (which is the actual error)
            preg_match('~\n(.*?)\n~', $input, $output);
            $jsonError = json_decode($output[1]);
            return response()->json($jsonError, $error->getCode());
        }

        return $this->responseHandler($response->getBody()->getContents());
    }

    /**
     * @param string $response  -   the json response string
     *
     * @return array|mixed
     */
    public function responseHandler($response)
    {
        if ($response)
            return response()->json($response, 200);

        return response()->json([], 200);
    }
}
