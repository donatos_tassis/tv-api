<?php


namespace App\Services;

use App\Helpers\TheMovieDbRoutes;
use App\Helpers\TheTvDbRoutes;
use App\Repositories\Interfaces\EpisodesRepositoryInterface;
use App\Repositories\Interfaces\SeriesRepositoryInterface;
use App\Series;
use GuzzleHttp\Client;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * Class TheMovieDbService
 *
 * @package App\Services
 */
class TheMovieDbService implements ApiServiceInterface
{
    /** @const string */
    const QUERY = 'query';

    /** @const string */
    const PAGE = 'page';

    /** @const string */
    const API_KEY = 'api_key';

    /** @const string */
    const SORT_BY = 'sort_by';

    /** @const string */
    const WITH_GENRES = 'with_genres';

    /** @const string */
    const VOTE_AVERAGE_GTE = 'vote_average.gte';

    /** @var TheMovieDbClient $client */
    protected $client;

    /** @var SeriesRepositoryInterface $seriesRepository */
    private $seriesRepository;

    /** @var EpisodesRepositoryInterface $episodesRepository */
    private $episodesRepository;

    /**
     * TheTvDbService constructor.
     *
     * @param Client $client
     * @param SeriesRepositoryInterface $seriesRepository
     * @param EpisodesRepositoryInterface $episodesRepository
     */
    public function __construct(
        Client $client,
        SeriesRepositoryInterface $seriesRepository,
        EpisodesRepositoryInterface $episodesRepository
    ) {
        $this->client = $client;
        $this->seriesRepository = $seriesRepository;
        $this->episodesRepository = $episodesRepository;
    }

    /**
     * Search series by name
     * the name must by passed as query parameter of the request
     *
     * @param Request $request
     *
     * @return array|mixed
     */
    public function searchSeries(Request $request)
    {
        $query = [];
        $query[self::QUERY] = $request->get(self::QUERY);
        $query[self::PAGE] = $request->get(self::PAGE);
        $query[self::API_KEY] = $request->get(self::API_KEY);
        $response = $this->client->requestAPI(
            TheMovieDbRoutes::SEARCH_SERIES,
            [self::QUERY => $query]
        );

        return $response->getOriginalContent();
    }

    /**
     * Discover series by a specified set of filters to be applied upon the search
     *
     * @param Request $request
     * @return mixed
     */
    public function discoverSeries(Request $request)
    {
        $query = [];
        $query[self::PAGE] = $request->get(self::PAGE);
        $query[self::API_KEY] = $request->get(self::API_KEY);
        if ($request->has(self::SORT_BY))
            $query[self::SORT_BY] = $request->get(self::SORT_BY);
        if ($request->has(self::WITH_GENRES))
            $query[self::WITH_GENRES] = $request->get(self::WITH_GENRES);
        if ($request->has(self::VOTE_AVERAGE_GTE))
            $query[self::VOTE_AVERAGE_GTE] = $request->get(self::VOTE_AVERAGE_GTE);
        $response = $this->client->requestAPI(
            TheMovieDbRoutes::DISCOVER_SERIES,
            [self::QUERY => $query]
        );

        return $response->getOriginalContent();
    }

    /**
     * List of all episodes for the specified series id
     *
     * @param Request $request
     * @param int $id           -   the series id
     * @param int $season       -   the season id
     * @param int $page         -   the page of the results
     * @param bool $addEpisodes -   boolean flag that determines if the database should be updated with new episodes
     *
     * @return array|mixed
     */
    public function episodes(Request $request, $id, $season = 0, $page = 1, $addEpisodes = true)
    {
//        $series = Series::find($id);
//        if(!$series)
//            return [];

        $query = [];
        $query[self::API_KEY] = $request->get(self::API_KEY);

        /** @var JsonResponse $response */
        $response = $this->client->requestAPI(
            TheMovieDbRoutes::SERIES . $id . TheMovieDbRoutes::SEASON . $season,
            [self::QUERY => $query]
        );

//        if($response->status() === 200 && $addEpisodes)
//        {
//            $pageData = json_decode($response->getOriginalContent(), true);
//            $episodes = $pageData['data'];
//
//            foreach ($episodes as $episode)
//                $this->episodesRepository->addEpisode($episode);
//
//            $next = $pageData['links']['next']; // link to the next page of episodes
//            if($next)
//                $this->episodes($id, $next);
//        }

        return $response->getOriginalContent();
    }

    /**
     * Get series information by id
     *
     * @param Request $request
     * @param int $id            -   the series id
     * @param bool $addSeries    -   boolean flag that determines if the database should be updated with new series
     *
     * @return array|mixed
     */
    public function series(Request $request, $id, $addSeries = true)
    {
        $query = [];
        $query[self::API_KEY] = $request->get(self::API_KEY);
        /** @var JsonResponse $response */
        $response = $this->client->requestAPI(
            TheMovieDbRoutes::SERIES . $id,
            [self::QUERY => $query]
        );

        // ToDo : need to add record in Database
//        if($response->status() === 200 && $addSeries)
//        {
//            $series = json_decode($response->getOriginalContent(), true);
//            $this->seriesRepository->addSeries($series);
//        }

        return $response->getOriginalContent();
    }

    /**
     * @param Request $request
     * @param $id                   -   the series id
     *
     * @return JsonResponse
     */
    public function videos(Request $request, $id)
    {
        $query = [];
        $query[self::API_KEY] = $request->get(self::API_KEY);
        /** @var JsonResponse $response */
        $response = $this->client->requestAPI(
            TheMovieDbRoutes::SERIES . $id . TheMovieDbRoutes::TRAILER,
            [self::QUERY => $query]
        );

        return $response->getOriginalContent();
    }

    /**
     * Get the most popular series
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function popular(Request $request)
    {
        $query = [];
        $query[self::API_KEY] = $request->get(self::API_KEY);
        $query[self::PAGE] = $request->get(self::PAGE);
        $response = $this->client->requestAPI(
            TheMovieDbRoutes::POPULAR_SERIES,
            [self::QUERY => $query]
        );

        return $response->getOriginalContent();
    }

    /**
     * Get the top rated series
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function topRated(Request $request)
    {
        $query = [];
        $query[self::API_KEY] = $request->get(self::API_KEY);
        $response = $this->client->requestAPI(
            TheMovieDbRoutes::TOP_RATED_SERIES,
            [self::QUERY => $query]
        );

        return $response->getOriginalContent();
    }

    /**
     * Gets all the genre categories for tv series
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function genreCategories(Request $request)
    {
        $query = [];
        $query[self::API_KEY] = $request->get(self::API_KEY);
        $response = $this->client->requestAPI(
            TheMovieDbRoutes::GENRE_CATEGORIES,
            [self::QUERY => $query]
        );

        return $response->getOriginalContent();
    }

    /**
     * Gets a list of series after user submitted filters are applied
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function filter(Request $request)
    {
        $params = $request->getContent();
        $filters = json_decode($params, true);

        $hasName = false;
        $hasMore = false;
        $queryParams = [];
        foreach ($filters as $filter) {
            switch ($filter['id']) {
                case self::QUERY:
                    $hasName = true;
                    $queryParams[self::QUERY] = $filter['value'];
                    break;
                case self::SORT_BY:
                    $queryParams[self::SORT_BY] = $filter['value'];
                    $hasMore = true;
                    break;
                case self::WITH_GENRES:
                    $queryParams[self::WITH_GENRES] = implode(',', $filter['value']);
                    $hasMore = true;
                    break;
                case 'rating':
                    $queryParams[self::VOTE_AVERAGE_GTE] = $filter['value'];
                    $hasMore = true;
                    break;
            }
        }

        if ($hasName && !$hasMore) {
            $response = $this->searchSeries($request->merge($queryParams));
        }

        if ($hasMore) {
            $response = $this->discoverSeries($request->merge($queryParams));

            if ($hasName) {
                $responseArray = json_decode($response, true);
                $filteredResults = [];
                foreach ($responseArray['results'] as $result) {
                    if (
                        stripos($result['name'], $queryParams[self::QUERY]) !== false ||
                        stripos($result['original_name'], $queryParams[self::QUERY]) !== false
                    )
                        $filteredResults[] = $result;
                }
                $responseArray['results'] = $filteredResults;
                $response = json_encode($responseArray);
            }
        }

        return $response;
    }
}
