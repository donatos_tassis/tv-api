<?php


namespace App\Services;


use Illuminate\Http\Request;

interface ApiServiceInterface
{
    public function searchSeries(Request $request);

    public function episodes(Request $request, $id, $season , $page, $addEpisodes);

    public function series(Request $request, $id, $addSeries);

    public function videos(Request $request, $id);

    public function popular(Request $request);

    public function topRated(Request $request);

    public function genreCategories(Request $request);

    public function filter(Request $request);
}
