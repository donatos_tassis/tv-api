<?php


namespace App\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Response;

/**
 * Class TheTvDbClient
 *
 * @package App\Services
 */
class TheMovieDbClient extends Client implements ApiClientInterface
{
    /** @var Client $client */
    protected $client;

    /**
     * TheTvDbClient constructor.
     *
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        parent::__construct();
        $this->client = $client;
    }

    /**
     * Request to TheTvDb API Client
     *
     * @param string $url
     * @param array $options
     * @param string $method
     *
     * @return array|mixed
     */
    public function requestAPI($url, $options = [], $method = 'GET')
    {
        $response = $this->client->request($method, $url, $options);

        return $this->responseHandler($response->getBody()->getContents());
    }

    /**
     * @param string $response  -   the json response string
     *
     * @return array|mixed
     */
    public function responseHandler($response)
    {
        if ($response)
            return response()->json($response, 200);

        return response()->json([], 200);
    }
}
