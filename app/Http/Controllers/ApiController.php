<?php

namespace App\Http\Controllers;


use App\Helpers\ApiServiceRegistry;
use App\Services\ApiServiceInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * Class ApiController
 *
 * @package App\Http\Controllers
 */
class ApiController extends Controller
{
    /** @var ApiServiceInterface $apiService */
    protected $apiService;

    /**
     * ApiController constructor.
     * @param ApiServiceRegistry $registry
     */
    public function __construct(ApiServiceRegistry $registry) {
        $serviceName = env('API_PROVIDER');
        $this->apiService = $registry->get($serviceName);
    }

    /**
     * Search series by name
     * the name must by passed as query parameter of the request
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function searchSeries(Request $request)
    {
        return $this->apiService->searchSeries($request);
    }

    /**
     * List of Most Popular series based on trend
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function popular(Request $request)
    {
        return $this->apiService->popular($request);
    }

    /**
     * List of Top Rated series based on user rating
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function topRated(Request $request)
    {
        return $this->apiService->topRated($request);
    }

    /**
     * List of all genre categories
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function genreCategories(Request $request)
    {
        return $this->apiService->genreCategories($request);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function filter(Request $request)
    {
        return $this->apiService->filter($request);
    }

    /**
     * List of all episodes for the specified series id
     *
     * @param Request $request
     * @param int $id           -   the series id
     * @param int $season       -   the season id
     * @param int $page         -   the page of the results
     * @param bool $addEpisodes -   boolean flag that determines if the database should be updated with new episodes
     *
     * @return JsonResponse
     */
    public function episodes(Request $request, $id, $season = 0, $page = 1, $addEpisodes = true)
    {
        return $this->apiService->episodes($request, $id, $season, $page, $addEpisodes);
    }

    /**
     * Get series information by id
     *
     * @param Request $request
     * @param int $id               -   the series id
     * @param bool $addSeries       -   boolean flag that determines if the database should be updated with new series
     *
     * @return JsonResponse
     */
    public function series(Request $request, $id, $addSeries = true)
    {
        return $this->apiService->series($request, $id, $addSeries);
    }

    /**
     * Gets a list of videos for the series with the given id
     *
     * @param Request $request
     * @param $id                   -   the series id
     *
     * @return JsonResponse
     */
    public function videos(Request $request, $id)
    {
        return $this->apiService->videos($request, $id);
    }
}
