<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;


/**
 * Class UserController
 *
 * @package App\Http\Controllers
 */
class UserController extends Controller
{
    /**
     * Register User through API
     *
     * @param RegisterRequest $request
     *
     * @return JsonResponse
     */
    public function register(RegisterRequest $request)
    {
        $userData = $request->validated();
        // ToDo: possible change: Remove bcrypt from server side and add it to client side
        $userData['password'] = bcrypt($userData['password']);

        /** @var User $user */
        $user = User::create($userData);
        $user->save();

        return response()->user($user, JsonResponse::HTTP_CREATED);
    }

    /**
     * Compares the received hash with the one stored into the database
     * for the given user (email)
     * returns true if they match otherwise returns false.
     *
     * @param LoginRequest $request
     *
     * @return JsonResponse
     */
    public function login(LoginRequest $request)
    {
        $loginData = $request->validated();
        $credentials = [
            'email' => $loginData['email'],
            'password' => $loginData['password']
        ];

        if(Auth::attempt($credentials)) {
            /** @var User $user */
            $user = Auth::user();

            // delete any existing tokens for this user
            foreach ($user->tokens as $token)
                $token->delete();

            $token = $user->createToken('auth_token');
            $user->withAccessToken($token->token);

            return response()->user($user, JsonResponse::HTTP_OK, true, $token->accessToken);
        }

        return response()->response('Invalid credentials', false, JsonResponse::HTTP_UNAUTHORIZED);
    }

    /**
     * Logout User though API (delete token)
     *
     * @return JsonResponse
     */
    public function logout()
    {
        /** @var User $user */
        $user = Auth::user();
        // delete any existing tokens for this user
        foreach ($user->tokens as $token)
            $token->delete();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Returns the users' profile information
     *
     * @return JsonResponse
     */
    public function profile()
    {
        return response()->json(Auth::user());
    }
}
