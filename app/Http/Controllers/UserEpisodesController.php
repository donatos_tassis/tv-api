<?php

namespace App\Http\Controllers;

use App\Episode;
use App\User;
use App\UserEpisode;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Class UserEpisodesController
 *
 * @package App\Http\Controllers
 */
class UserEpisodesController extends Controller
{
    /**
     * Updates the watched status of the given episode id for the authenticated user.
     * A boolean parameter should be passed in the query properties e.g. watched=1 / watched=0
     *
     * @param Request $request      -   the request
     * @param int $id               -   the episode id to be updated
     *
     * @return JsonResponse
     */
    public function watched(Request $request, $id)
    {
        $request->validate(['watched' => 'required|boolean']);

        $episode = Episode::find($id);
        if(!$episode)
            return response()->response('Episode id not found', false, JsonResponse::HTTP_NOT_FOUND);

        /** @var User $user */
        $user = Auth::user();
        // search the database for record with episode id and user id
        $userEpisode = UserEpisode::where('user_id', $user->id)->where('episode_id', $episode->id)->first();

        if(!$userEpisode)
            return response()->response('Episode not found in users\' favorites list', false);

        $watched = $request->input('watched');
        $userEpisode->update(['watched' => $watched]);

        return response()->response('Watch status updated');
    }
}
