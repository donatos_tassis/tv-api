<?php

namespace App\Http\Controllers;

use App\Episode;
use App\Http\Resources\UserEpisodeResource;
use App\Http\Resources\UserSeriesResource;
use App\Series;
use App\User;
use App\UserEpisode;
use App\UserSeries;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

/**
 * Class UserSeriesController
 *
 * @package App\Http\Controllers
 */
class UserSeriesController extends Controller
{
    /**
     * Add a series to the list of the users' favorites
     *
     * @param int $id   -   the series id to be added
     *
     * @return JsonResponse
     */
    public function add($id)
    {
        $series = Series::find($id);
        if(!$series)
            return response()->response('Series id not found', false, JsonResponse::HTTP_NOT_FOUND);

        /** @var User $user */
        $user = Auth::user();
        // search the database for record with series id and user id
        $userSeries = UserSeries::where('user_id', $user->id)->where('series_id', $series->id)->first();

        if($userSeries) // if userSeries record exists it is already added in the list
            return response()->response('Series already in favorites list', false, JsonResponse::HTTP_BAD_REQUEST);

        $userSeriesData = [
            'user_id'   => $user->id,
            'series_id' => $series->id
        ];

        $userSeriesResource = UserSeriesResource::make($userSeriesData)->resolve();
        $userSeries = UserSeries::create($userSeriesResource);
        $userSeries->save();    // adds the series in the database
        $this->addEpisodes($user, $userSeries->series_id);  // adds the episodes of the series into the database

        return response()->response('Series successfully added in the list of favorites');
    }

    /**
     * Removes a series from the list of the users' favorites
     *
     * @param int $id   -   the series id to be removed
     *
     * @return JsonResponse
     */
    public function remove($id)
    {
        $series = Series::find($id);
        if(!$series)
            return response()->response('Series id not found', false, JsonResponse::HTTP_NOT_FOUND);

        /** @var User $user */
        $user = Auth::user();
        // search the database for record with series id and user id
        $userSeries = UserSeries::where('user_id', $user->id)->where('series_id', $series->id)->first();

        if(!$userSeries)
            return response()->response('Series not found in the list of favorites', false, JsonResponse::HTTP_NOT_FOUND);

        $userSeries->delete(); // removes the series from the database
        $this->removeEpisodes($user, $userSeries->series_id); // removes the episodes of the series from the database

        return response()->response('Series successfully removed from the list of favorites');
    }

    /**
     * Adds the episodes of the specified series id into the UserEpisodes table
     * which holds the users' favorite episode list and their status ( watched or not )
     *
     * @param User $user          -   The User
     * @param int $series         -   The series id
     */
    protected function addEpisodes(User $user, $series)
    {
        $episodes = Episode::all()->where('series_id', $series);
        foreach ($episodes as $episode) {
            $userEpisode = UserEpisode::where('user_id', $user->id)->where('episode_id', $episode->id)->first();
            if($userEpisode) continue;

            $userEpisodeData = [
                'user_id'   => $user->id,
                'episode_id' => $episode->id
            ];

            $userEpisodeResource = UserEpisodeResource::make($userEpisodeData)->resolve();
            $userEpisode = UserEpisode::create($userEpisodeResource);
            $userEpisode->save();
        }
    }

    /**
     * Removes the episode of the specified series id from the UserEpisodes table
     * which hold the users' favorite episode list and their status ( watched or not )
     *
     * @param User $user        -   The User
     * @param int $series       -   The series id
     */
    protected function removeEpisodes(User $user, $series)
    {
        $episodes = Episode::all()->where('series_id', $series);
        foreach ($episodes as $episode) {
            $userEpisode = UserEpisode::where('user_id', $user->id)->where('episode_id', $episode->id)->first();
            if(!$userEpisode) continue;

            $userEpisode->delete();
        }
    }
}
