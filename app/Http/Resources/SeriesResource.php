<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class SeriesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'           => $this->resource['id'],
            'name'         => $this->resource['seriesName'],
            'banner'       => $this->resource['banner'],
            'status'       => $this->resource['status'],
            'overview'     => array_key_exists('overview', $this->resource) ? $this->resource['overview'] : null,
            'genre'        => array_key_exists('genre', $this->resource) ? $this->resource['genre'] : null,
            'last_updated' => array_key_exists('lastUpdated', $this->resource) ? $this->resource['lastUpdated'] : 0,
        ];
    }
}
