<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class EpisodeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->resource['id'],
            'series_id'     => $this->resource['seriesId'],
            'season_id'     => $this->resource['airedSeasonID'],
            'name'          => empty($this->resource['episodeName']) ? '' : $this->resource['episodeName'],
            'season'        => $this->resource['airedSeason'],
            'episode'       => $this->resource['airedEpisodeNumber'],
            'overview'      => $this->resource['overview'],
            'air_date'      => empty($this->resource['firstAired']) || "0000-00-00" ? null : $this->resource['firstAired'],
            'last_updated'  => $this->resource['lastUpdated'],
        ];
    }
}
