<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class UserSeriesResource
 *
 * @package App\Http\Resources
 */
class UserSeriesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'user_id'   => $this->resource['user_id'],
            'series_id' => $this->resource['series_id'],
        ];
    }
}
