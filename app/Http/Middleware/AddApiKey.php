<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AddApiKey
{
    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $apiKey = env('THEMOVIEDB_API_KEY');
        $request->query->add(['api_key' => $apiKey]);

        return $next($request);
    }
}
