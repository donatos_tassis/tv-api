<?php

namespace App\Http\Requests;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'      => 'required|string|max:255',
            'surname'   => 'required|string|max:255',
            'email'     => 'required|string|email|max:255|unique:users',
            'password'  => 'required|string',
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required'     => 'The name is required.',
            'name.surname'      => 'The surname is required.',
            'email.required'    => 'The email is required.',
            'email.email'       => 'This is not a valid email.',
            'email.unique'      => 'Email already in use.',
            'password.required' => 'The password is required.',
        ];
    }
}
