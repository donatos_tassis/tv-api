<?php

namespace App\Providers;

use App\Helpers\TheTvDbRoutes;
use App\Services\ApiClientInterface;
use App\Services\TheTvDbClient;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\ServiceProvider;

/**
 * Class GuzzleProvider
 *
 * @package App\Providers
 */
class TheTvDbProvider extends ServiceProvider
{
    /** @const string */
    const API_TOKEN = 'api_token';

    /**
     * The expiration of the token in minutes (1 day) stored in cache memory
     * (the same value as set in TheTvDb API)
     *
     * @const int
     */
    const MINUTES = 1440;

    /** @var string $baseUrl */
    protected $baseUrl;

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->baseUrl = $baseUrl = TheTvDbRoutes::API_BASE_URL;
        $token = $this->getToken();

        $this->app->singleton(ApiClientInterface::class, function($api) use ($baseUrl, $token) {
            return new TheTvDbClient(
                new Client([
                    'base_uri' => $baseUrl,
                    'headers' => [
                        'Authorization' => 'Bearer ' . $token,
                    ]
                ])
            );
        });
    }

    /**
     * Gets the token either from the cache memory or from the API
     *
     * @return string
     */
    private function getToken()
    {
        if (Cache::has(self::API_TOKEN)) {
            return Cache::get(self::API_TOKEN);
        }

        return $this->regenToken();
    }

    /**
     * Regenerate the token by requesting the API
     *
     * @return string
     */
    private function regenToken()
    {
        $body = [
            "apikey" => env('THETVDB_API_KEY'),
            "userkey" => env('THETVDB_USER_KEY'),
            "username" => env('THETVDB_USERNAME')
        ];

        $client = new Client([
            'base_uri' => $this->baseUrl,
            'headers' => [
                'Content-Type' => 'application/json'
            ]
        ]);

        $jsonResponse = $client->post( TheTvDbRoutes::LOGIN, [ 'json' => $body ]);
        $response = json_decode($jsonResponse->getBody()->getContents());
        $token = $response->token;
        Cache::put(self::API_TOKEN, $token, self::MINUTES); // store token into cache memory

        return $token;
    }
}
