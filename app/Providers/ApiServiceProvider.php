<?php

namespace App\Providers;

use App\Helpers\ApiServiceRegistry;
use App\Repositories\Interfaces\EpisodesRepositoryInterface;
use App\Repositories\Interfaces\SeriesRepositoryInterface;
use App\Services\ApiClientInterface;
use App\Services\TheMovieDbService;
use App\Services\TheTvDbService;
use Illuminate\Support\ServiceProvider;

class ApiServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(ApiServiceRegistry::class);
    }

    /**
     * Bootstrap services.
     *
     * @param SeriesRepositoryInterface $seriesRepository
     * @param EpisodesRepositoryInterface $episodesRepository
     * @return void
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function boot(SeriesRepositoryInterface $seriesRepository, EpisodesRepositoryInterface $episodesRepository)
    {
        $client = $this->app->get(ApiClientInterface::class);
        $this->app->make(ApiServiceRegistry::class)
            ->register('tv', new TheTvDbService($client, $seriesRepository, $episodesRepository));
        $this->app->make(ApiServiceRegistry::class)
            ->register('movie', new TheMovieDbService($client, $seriesRepository, $episodesRepository));
    }
}
