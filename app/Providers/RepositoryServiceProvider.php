<?php

namespace App\Providers;

use App\Repositories\EpisodesRepository;
use App\Repositories\Interfaces\EpisodesRepositoryInterface;
use App\Repositories\Interfaces\SeriesRepositoryInterface;
use App\Repositories\SeriesRepository;
use Illuminate\Support\ServiceProvider;

/**
 * Class RepositoryServiceProvider
 *
 * @package App\Providers
 */
class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            SeriesRepositoryInterface::class,
            SeriesRepository::class
        );

        $this->app->bind(
            EpisodesRepositoryInterface::class,
            EpisodesRepository::class
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
