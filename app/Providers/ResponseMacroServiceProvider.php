<?php

namespace App\Providers;

use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\ServiceProvider;

class ResponseMacroServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Response::macro(
            'user',
            function (User $user, $status = JsonResponse::HTTP_OK, $isAuthenticated = false, $token = null) {
            return Response::json([
                'success' => $isAuthenticated,
                'data' => [
                    'id'            => $user->id,
                    'name'          => $user->name,
                    'surname'       => $user->surname,
                    'email'         => $user->email,
                    'auth_token'    => $token,
                ]
            ], $status);
        });

        Response::macro('response', function ($message, $success = true, $status = JsonResponse::HTTP_OK) {
            return Response::json([
                'success'   => $success,
                'message'   => $message,
            ], $status);
        });
    }
}
