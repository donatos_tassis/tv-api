<?php

namespace App\Providers;

use App\Helpers\TheMovieDbRoutes;
use App\Services\ApiClientInterface;
use App\Services\TheMovieDbClient;
use GuzzleHttp\Client;
use Illuminate\Support\ServiceProvider;

class TheMovieDbProvider extends ServiceProvider
{
     /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->baseUrl = $baseUrl = TheMovieDbRoutes::API_BASE_URL;

        $this->app->singleton(ApiClientInterface::class, function($api) use ($baseUrl) {
            return new TheMovieDbClient(
                new Client([
                    'base_uri' => $baseUrl
                ])
            );
        });
    }
}
