<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $apiProvider = env('API_PROVIDER') ?? 'movie';
        switch ($apiProvider) {
            case 'tv':
                $this->app->register(TheTvDbProvider::class);
                break;
            case 'movie':
                $this->app->register(TheMovieDbProvider::class);
                break;
        }

        $this->app->register(ApiServiceProvider::class);
    }
}
