<?php

namespace App\Repositories;

use App\Http\Resources\SeriesResource;
use App\Repositories\Interfaces\SeriesRepositoryInterface;
use App\Series;

class SeriesRepository implements SeriesRepositoryInterface
{
    /**
     * Adds into the database any series not yet stored
     *
     * @param array $series -   array that contains series data
     */
    public function addSeries(array $series)
    {
        if(Series::find($series['id']))
            return;

        $seriesData = SeriesResource::make($series)->resolve();
        $seriesEntity = Series::create($seriesData);
        $seriesEntity->save();
    }
}
