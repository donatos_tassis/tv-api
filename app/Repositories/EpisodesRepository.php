<?php

namespace App\Repositories;

use App\Episode;
use App\Http\Resources\EpisodeResource;
use App\Repositories\Interfaces\EpisodesRepositoryInterface;

class EpisodesRepository implements EpisodesRepositoryInterface
{

    /**
     * Adds into the database any episode not yet stored
     *
     * @param array $episode  -  array that contains episodes' data
     */
    public function addEpisode(array $episode)
    {
        if(Episode::find($episode['id']))
            return;
        $episodeData = EpisodeResource::make($episode)->resolve();
        $episodeEntity = Episode::create($episodeData);
        $episodeEntity->save();
    }
}
