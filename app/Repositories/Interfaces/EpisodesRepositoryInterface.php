<?php


namespace App\Repositories\Interfaces;

interface EpisodesRepositoryInterface
{
    /**
     * Adds into the database any episode not yet stored
     *
     * @param array $episode  -  array that contains episodes' data
     */
    public function addEpisode(array $episode);
}
