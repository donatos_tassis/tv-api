<?php

namespace App\Repositories\Interfaces;

interface SeriesRepositoryInterface
{
    /**
     * Adds into the database any series not yet stored
     *
     * @param array $series -   array that contains series data
     */
    public function addSeries(array $series);
}
