<?php

namespace App\Exceptions;

use Exception;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($exception instanceof GuzzleException && $request->wantsJson()) {
            $input = $exception->getMessage();
            // get the string between the 2 line breaks (which is the actual error)
            preg_match('~\n(.*?)\n~', $input, $output);
            $jsonError = json_decode($output[1], true);
            return response()->json($jsonError, $exception->getCode());
        }

        return parent::render($request, $exception);
    }
}
