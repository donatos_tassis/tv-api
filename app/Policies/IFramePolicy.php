<?php


namespace App\Policies;


use Spatie\Csp\Directive;
use Spatie\Csp\Policies\Basic;

class IFramePolicy extends Basic
{
    public function configure()
    {
        $domain = env('MIX_CLIENT_URL');
        $this->addDirective(Directive::FRAME_ANCESTORS, $domain);
    }
}
