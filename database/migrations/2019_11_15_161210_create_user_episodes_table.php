<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Migration file that creates the UserEpisodes entity
 *
 * Class CreateUserEpisodesTable
 */
class CreateUserEpisodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_episodes', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('episode_id');
            $table->primary(['user_id', 'episode_id']);
            $table->boolean('watched')->default(false);
            $table->foreign('user_id', 'user_episodes_user_id_foreign')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->foreign('episode_id','user_episodes_episode_id_foreign')
                ->references('id')
                ->on('episodes')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_episodes');
    }
}
