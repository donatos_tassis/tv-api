<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Migration file that creates the UserSeries entity
 *
 * Class CreateUserSeriesTable
 */
class CreateUserSeriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_series', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('series_id');
            $table->primary(['user_id', 'series_id']);
            $table->foreign('series_id', 'user_series_series_id_foreign')
                ->references('id')
                ->on('series')
                ->onDelete('cascade');
            $table->foreign('user_id','user_series_user_id_foreign')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_series');
    }
}
