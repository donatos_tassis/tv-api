<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

$shouldAddApiKey = env('THEMOVIEDB_API_KEY') === 'movie';

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => 'api-header'], function ($shouldAddApiKey) {

    Route::group(['prefix' => 'user'], function() {
        Route::post('/register', 'UserController@register');
        Route::post('/login', 'UserController@login');
    });

    Route::group(['prefix' => 'search'], function($shouldAddApiKey) {
        $searchSeries = Route::get('/series', 'ApiController@searchSeries');
        if($shouldAddApiKey)
            $searchSeries->middleware('api-key');
    });

    Route::group(['prefix' => 'series'], function($shouldAddApiKey) {
        $routes = [];
        $routes[] = Route::get('/popular', 'ApiController@popular');
        $routes[] = Route::get('/top-rated', 'ApiController@topRated');
        $routes[] = Route::get('/genre/list', 'ApiController@genreCategories');
        $routes[] = Route::post('/filter', 'ApiController@filter');
        $routes[] = Route::get('/{id}', 'ApiController@series');
        $routes[] = Route::get('/{id}/season/{season}', 'ApiController@episodes');
        $routes[] = Route::get('/{id}/videos', 'ApiController@videos');
        if($shouldAddApiKey) {
            foreach ($routes as $route)
                $route->middleware('api-key');
        }
    });

    // routes that needs authentication
    Route::group(['middleware' => 'auth:api', 'prefix' => 'user'], function() {

        Route::get('/logout', 'UserController@logout');
        Route::get('/profile', 'UserController@profile');

        Route::get('/series/{id}/add', 'UserSeriesController@add');
        Route::get('/series/{id}/remove', 'UserSeriesController@remove');

        Route::get('/series', 'UserSeriesController@index');
        Route::get('/series/{id}/episodes', 'UserEpisodesController@index');

        Route::get('/episodes/{id}', 'UserEpisodesController@watched');
    });
});
